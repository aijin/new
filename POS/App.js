import * as firebase from 'firebase';
import React, {Component} from 'react';
import {View, Alert} from 'react-native';
import Header from './screens/Components/Header';
import ListItem from './screens/Components/ListItem';
import Button from './screens/Components/Button';
import ListView from 'deprecated-react-native-listview';
const config = {
  apiKey: 'AIzaSyAH0vYjNk7IlslnTC7DrOVNtCEC--V_qK8',
  authDomain: 'point-of-sale-8870c.firebaseapp.com',
  databaseURL: 'https://point-of-sale-8870c.firebaseio.com/',
  projectId: 'point-of-sale-8870c',
  storageBucket: 'point-of-sale-8870c.appspot.com',
};
firebase.initializeApp(config);
var database = firebase.database();
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
    };
    this.itemsRef = database.ref('inventorySimple');
  }
  componentDidMount() {
    this.listenForItem(this.itemsRef);
  }
  componentDidUnMount() {
    this.state.itemsRef.off('value');
  }
  render() {
    return (
      <View style={styles.container}>
        <Header title="Inventory Simple" />
        <ListView
          dataSource={this.state.dataSource}
          renderRow={this.renderItem.bind(this)}
          enableEmptySections={true}
          style={styles.listview}
        />
        <Button title="Add Item" onPress={this.addItem.bind(this)} />
      </View>
    );
  }
  renderItem(item) {
    const onPress = () => {
      Alert.alert(
        'Delete Item',
        'delete ' + item.title + ' ?',
        [
          {
            text: 'Cancel',
            onPress: () => console.log('cancel Pressed'),
            style: 'cancel',
          },
          {text: 'OK', onPress: () => this.itemsRef.child(item._key).remove()},
        ],
        {cancelable: false},
      );
    };
    const onLongPress = () => {
      Alert.alert('Edit Item', 'current item is ' + item.title, [
        {
          text: 'Cancel',
          onPress: () => console.log('cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: text => this.itemsRef.child(item._key).update({title: text}),
        },
      ]);
    };
    return <ListItem item={item} onPress={onPress} onLongPress={onLongPress} />;
  }
  listenForItem(itemsRef) {
    itemsRef.on('value', snap => {
      //get array of children
      var items = [];
      snap.forEach(child => {
        items.push({
          title: child.val().title,
          _key: child.key,
        });
      });
      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(items),
      });
    });
  }
  addItem() {
    Alert.alert('Add Item', null, [
      {
        text: 'Ask me later',
        onPress: () => console.log('Ask me later pressed'),
      },
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {text: 'OK', onPress: text => this.itemsRef.push({title: text})},
    ]);
  }
}
const styles = {
  container: {
    backgroundColor: '#f2f2f2',
    flex: 1,
  },
  listview: {
    flex: 1,
  },
};
